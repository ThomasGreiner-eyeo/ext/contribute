"use strict";

{
  const listeners = new Map([
    ["load", []],
    ["unload", []]
  ]);

  window.$ = (selector) => document.querySelector(selector);

  window.emit = (name) =>
  {
    if (!listeners.has(name))
      return;

    for (const listener of listeners.get(name))
    {
      listener();
    }
  };

  window.on = (name, listener) =>
  {
    if (!listeners.has(name))
      return;

    listeners.get(name).push(listener);
  };

  function onUnload()
  {
    port.onDisconnect.removeListener(onUnload);
    emit("unload");
    listeners.clear();
  }

  const port = chrome.runtime.connect();
  port.onDisconnect.addListener(onUnload);
  setTimeout(() =>
  {
    emit("load");
  }, 0);
}

"use strict";

{
  function getYouTubeAuthor()
  {
    const name = $("#text > a");
    if (!name)
      return null;

    return name.textContent;
  }

  function getAuthorRel()
  {
    const link = $("[rel='author']");
    if (!link)
      return null;

    return link.textContent;
  }

  function getAuthorMeta()
  {
    const name = $("meta[name='author']");
    if (!name)
      return null;

    return name.getAttribute("content");
  }

  function getAuthorSchemaOrg()
  {
    const name = $("[itemprop='author'] > [itemprop='name']");
    if (!name)
      return null;

    return name.getAttribute("content");
  }

  function getAuthor()
  {
    let name = getYouTubeAuthor();

    if (!name)
    {
      name = getAuthorSchemaOrg();
    }

    if (!name)
    {
      name = getAuthorRel();
    }

    if (!name)
    {
      name = getAuthorMeta();
    }

    if (!name)
    {
      name = location.hostname;
    }

    return {name};
  }

  function onMessage(msg, sender, sendResponse)
  {
    if (msg !== "author")
      return;

    const author = getAuthor();
    sendResponse(author);
  }

  on("load", () =>
  {
    chrome.runtime.onMessage.addListener(onMessage);
  });

  on("unload", () =>
  {
    chrome.runtime.onMessage.removeListener(onMessage);
  });
}

"use strict";

{
  const prioritizedActions = ["donate", "support"];
  const urgentOptions = [
    "donate.gofundme",
    "support.kickstarter"
  ];

  const knownOptions = {
    "": [
      {
        id: "follow.facebook",
        selector: "a[href*='://www.facebook.com/']",
        matchLink: /^https?:\/\/www\.facebook\.com\/([a-zA-Z0-9_\-\.]+)\b/,
        excludedDomains: ["facebook.com"]
      },
      {
        id: "follow.github",
        selector: "a[href*='://github.com/']",
        matchLink: /^https?:\/\/github\.com\/([a-zA-Z0-9_\-\.]+)\b/,
        excludedDomains: ["github.com"]
      },
      {
        id: "follow.gitlab",
        selector: "a[href*='://gitlab.com/']",
        matchLink: /^https?:\/\/gitlab\.com\/([a-zA-Z0-9_\-\.]+)\b/,
        excludedDomains: ["gitlab.com"]
      },
      {
        id: "follow.instagram",
        selector: "a[href*='://www.instagram.com/']",
        matchLink: /^https?:\/\/www\.instagram\.com\/([a-zA-Z0-9_\-\.]+)\b/,
        excludedDomains: ["instagram.com"]
      },
      {
        id: "follow.linkedin",
        selector: "a[href*='://www.linkedin.com/']",
        matchLink: /^https?:\/\/www\.linkedin\.com\/(?:company|in)\/([a-zA-Z0-9_\-\.]+)\b/,
        excludedDomains: ["linkedin.com"]
      },
      {
        id: "follow.twitter",
        selector: "a[href*='://twitter.com/']",
        matchLink: /^https?:\/\/twitter\.com\/(?!search)([a-zA-Z0-9_\-\.]+)\b/,
        excludedDomains: ["twitter.com"]
      },
      {
        id: "follow.vk",
        selector: "a[href*='://vk.com/']",
        matchLink: /^https?:\/\/vk\.com\/([a-zA-Z0-9_\-\.]+)\b/,
        excludedDomains: ["vk.com"]
      },
      {
        id: "subscribe.youtube",
        selector: "a[href*='youtube.com/']",
        matchLink: /^https?:\/\/(?:www\.)youtube\.com\/(?:user\/)?([a-zA-Z0-9_\-\.]+)\b/,
        excludedDomains: ["youtube.com"]
      }
    ],
    "com": {
      "youtube": {
        "": [
          {
            id: "follow.facebook",
            selector: "#description a[href*='www.facebook.com']",
            matchParams: /^https?:\/\/www\.facebook\.com\/([a-zA-Z0-9_\-\.]+)\b/
          },
          {
            id: "donate.gofundme",
            selector: "#description a[href*='www.gofundme.com']",
            matchParams: /^https?:\/\/www\.gofundme\.com\/f\/([a-zA-Z0-9_\-\.]+)\b/
          },
          {
            id: "support.kickstarter",
            selector: "#description a[href*='www.kickstarter.com']",
            matchParams: /^https?:\/\/www\.kickstarter\.com\/projects\/([a-zA-Z0-9_\-\.]+)\/([a-zA-Z0-9_\-\.]+)?(?:\?|$)/
          },
          {
            id: "follow.instagram",
            selector: "#description a[href*='www.instagram.com']",
            matchParams: /^https?:\/\/www\.instagram\.com\/([a-zA-Z0-9_\-\.]+)\b/
          },
          {
            id: "donate.patreon",
            selector: "#description a[href*='www.patreon.com']",
            matchParams: /^https?:\/\/www\.patreon\.com\/([a-zA-Z0-9_\-\.]+)\b/
          },
          {
            id: "buy.teespring",
            selector: "#description a[href*='teespring.com']",
            matchParams: /^https?:\/\/teespring\.com\/stores\/([a-zA-Z0-9_\-\.]+)\b/
          },
          {
            id: "follow.twitter",
            selector: "#description a[href*='twitter.com']",
            matchParams: /^https?:\/\/twitter\.com\/(?!search)([a-zA-Z0-9_\-\.]+)\b/
          }
        ]
      }
    }
  };

  let foundOptions = [];
  // TODO: check again whenever content changes instead of at an interval
  let refreshInterval;

  function crawlOptions(obj, hostParts)
  {
    const options = [];

    if ("" in obj)
    {
      options.push(...obj[""]);
    }

    let part = hostParts.pop();
    if (part && part in obj)
    {
      options.push(...crawlOptions(obj[part], hostParts));
    }

    return options;
  }

  function getOptions()
  {
    const options = [];
    const optionsByUrl = new Map();

    const domainOptions = crawlOptions(
      knownOptions,
      location.hostname.split(".")
    );

    for (const knownOption of domainOptions)
    {
      const elements = document.querySelectorAll(knownOption.selector);
      if (!elements.length)
        continue;

      for (const element of elements)
      {
        if (knownOption.excludedDomains)
        {
          const domain = location.hostname.replace(/^www\./, "");
          if (knownOption.excludedDomains.includes(domain))
            continue;
        }

        let info;

        if (knownOption.matchLink)
        {
          const match = knownOption.matchLink.exec(element.href);
          if (!match)
            continue;

          const [url, id] = match;
          info = {id, url};
        }
        else if (knownOption.matchParams)
        {
          const params = new URLSearchParams(element.search);
          const href = params.get("q");
          const match = knownOption.matchParams.exec(href);
          if (!match)
            return null;

          const [url, id] = match;
          info = {id, url};
        }

        if (!info)
          continue;

        let option = optionsByUrl.get(info.url);
        if (!option)
        {
          option = {
            id: knownOption.id,
            info,
            count: 0
          };
          options.push(option);
          optionsByUrl.set(info.url, option);
        }
        option.count++;
      }
    }

    options.sort((a, b) => b.count - a.count);

    return options;
  }

  function getPriority(options)
  {
    let priority = 0;

    for (const option of options)
    {
      let optionPriority = 0;
      if (urgentOptions.includes(option.id))
      {
        optionPriority = 2;
      }
      else
      {
        const [action] = option.id.split(".", 1);
        if (prioritizedActions.includes(action))
        {
          optionPriority = 1;
        }
      }

      if (optionPriority <= priority)
        continue;

      priority = optionPriority;
    }

    return priority;
  }

  function refreshOptions()
  {
    foundOptions = getOptions();

    if (!foundOptions.length)
      return;

    chrome.runtime.sendMessage({
      type: "counter.set",
      priority: getPriority(foundOptions),
      value: foundOptions.length
    });
  }

  refreshOptions();

  function onMessage(msg, sender, sendResponse)
  {
    if (msg.type !== "options.get")
      return;

    refreshOptions();
    sendResponse(foundOptions);
  }

  on("load", () =>
  {
    refreshInterval = setInterval(refreshOptions, 1000);
    chrome.runtime.onMessage.addListener(onMessage);
  });

  on("unload", () =>
  {
    clearInterval(refreshInterval);
    chrome.runtime.onMessage.removeListener(onMessage);
  });
}

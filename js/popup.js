"use strict";

const labelsByAction = new Map([
  ["buy", "Buy"],
  ["donate", "Donate"],
  ["follow", "Follow"],
  ["subscribe", "Subscribe"],
  ["support", "Support"]
]);

const $ = (selector) => document.querySelector(selector);

function create(parent, tagName)
{
  const element = document.createElement(tagName);
  parent.appendChild(element);
  return element;
}

function getOptionLabel(option)
{
  const [action] = option.id.split(".", 1);
  const template = labelsByAction.get(action) || "$1";
  const ids = [option.info.id].flat();

  let label = template;
  for (let i = 0; i < ids.length; i++)
  {
    label = label.replace(`$${i + 1}`, ids[i]);
  }
  return label;
}

chrome.tabs.query({active: true, lastFocusedWindow: true}, (tabs) =>
{
  chrome.tabs.sendMessage(tabs[0].id, "author", (author) =>
  {
    if (!author)
      return;

    $("#author").textContent = author.name;
  });

  chrome.tabs.sendMessage(tabs[0].id, {type: "options.get"}, (options) =>
  {
    if (!options)
    {
      $("#options").textContent = "No contribution options found";
      return;
    }

    const groups = new Map();

    for (const option of options)
    {
      const [action, destination] = option.id.split(".", 2);

      if (!labelsByAction.has(action))
      {
        console.warn("Invalid action", action);
        continue;
      }

      let group = groups.get(action);
      if (!group)
      {
        const groupContainer = create($("#options"), "div");
        groupContainer.classList.add("options-group");

        const groupLabel = create(groupContainer, "button");
        groupLabel.classList.add("label");
        groupLabel.textContent = labelsByAction.get(action);

        group = create(groupContainer, "ul");
        groups.set(action, group);
      }

      const item = create(group, "li");
      item.classList.add(`action-${action}`);
      item.classList.add(`dest-${destination}`);
      item.style.setProperty("--significance", option.count);

      const link = create(item, "a");
      link.target = "_blank";
      link.href = option.info.url;
      link.textContent = option.info.id;
    }
  });
});

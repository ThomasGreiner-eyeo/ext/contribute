"use strict";

const contentScripts = [];
const manifest = chrome.runtime.getManifest();
const colorByPriority = [
  "#9BB0A4",
  "#52B788",
  "#D00000"
];

// We have to ensure there is at least one listener for the onConnect event.
// Otherwise we can't connect a port later, which we need to do in order to
// detect when the extension is reloaded, disabled or uninstalled.
chrome.runtime.onConnect.addListener(() => {});

for (const {js: files, runAt} of manifest.content_scripts)
{
  for (const file of files)
  {
    contentScripts.push({file, runAt});
  }
}

chrome.action.setBadgeBackgroundColor({color: "#52B788"});
chrome.runtime.onMessage.addListener((msg, sender) =>
{
  if (msg.type !== "counter.set")
    return;

  chrome.action.setBadgeBackgroundColor({
    color: colorByPriority[msg.priority || 0],
    tabId: sender.tab.id
  });

  chrome.action.setBadgeText({
    tabId: sender.tab.id,
    text: msg.value.toString()
  });
});

chrome.tabs.query({}, (tabs) =>
{
  for (const tab of tabs)
  {
    if (tab.id === chrome.tabs.TAB_ID_NONE)
      continue;

    if (!/^https?:/.test(tab.url))
      continue;

    // Inject content scripts into existing tabs
    for (const script of contentScripts)
    {
      chrome.tabs.executeScript(tab.id, script);
    }
  }
});

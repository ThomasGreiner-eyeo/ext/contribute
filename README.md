## Examples

- Homepage:
  - https://www.greinr.com/
  - https://adblockplus.org/
  - https://getadblock.com/
- Medium: https://medium.com/live-your-life-on-purpose/three-life-lessons-from-a-dying-man-d0c08c0fe030
- YouTube
  - Facebook: https://www.youtube.com/watch?v=FkyQqAmnRdE
  - GoFundMe: https://www.youtube.com/watch?v=CgoSDj6AQGc
  - Instagram: https://www.youtube.com/watch?v=Xc4xYacTu-E
  - Kickstarter: https://www.youtube.com/watch?v=28YwvwneKoY
  - Patreon: https://www.youtube.com/watch?v=-sDmZtYQ9bE
  - Teespring: https://www.youtube.com/watch?v=-sDmZtYQ9bE
  - Twitter: https://www.youtube.com/watch?v=Xc4xYacTu-E

## Permissions

- `tabs`: Required for reading tab URL when injecting content scripts during initialization.
- Host permissions: Required for injecting content scripts during initialization.

## Assets

- Icons: https://www.iconfinder.com/iconsets/2018-social-media-logotypes

## TODO

- Detect web payment API
